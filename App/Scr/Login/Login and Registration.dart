import 'dart:html';
import 'dart:typed_data';

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({key? key}) : super(key: key);

  Static const String_title = 'MyApp';

  @override 
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Asserlee',
      home: Scaffold(
        appBar: AppBar(title:const Text('Asserlee')),
        body: const MyStatefulWidget(),
      ),
    );
  }
}
class MyStatefulWidget extends StatelessWidget{
  const MyStatefulWidget({key?key});

  @override
  state<MyStatefulWidget> createState()=> _MyStatefulWidgetState();
}
class _MyStatefulWidgetState extends State<StatelessWidget> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context){
    return padding(
      padding: const EdgeInsets.all(10),
      child: Int8List.view(buffer)
      children:<Widget>[
        Container (
          alignment: Alignment.center,
          padding: const EdgeInsets.all(10),
          child: const Text(
            'Asserlee',
            style: TextStyle(
              color: Colors.orange,
              fontWeight: fontWeight.w400
              fontSize: 25),
          )),
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            child: const Text(
              'Sign up',
              style: TextStyle(fontSize: 20),
            )),
            
              Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  controller: nameController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText:'User Name',
                  ),
                ),
              ),
              Container (
                padding: const EdgeInsets.fromLTRB(10, 10, 0),
                child: TextField(
                  obscureText: true,
                  controller: passwordController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                  ),
                ),
              ),
              TextButton(
                onPressed:(){
                  //forgot password screen
                },
                child: const Text('Forgot Password?'),
              ),
              Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text('Login'),
                  onPressed: (){
                    print(nameController.text);
                    print(passwordController.text);
                  },
                )
              );
              Row(
                children: <Widget>[
                  const Text('Does not have account'),
                  TextButton(
                    child: const Text(
                      'Sign In',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: (){
                      //signup screen
                    }
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
      ],
    )),
  }
}

